#!/bin/env python3
import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

requires = ['websockets', 
            'shortuuid',
           ]

setup(name='tulipfest',
      version='0.0.1',
      description='An exploration of the AsyncIO (aka Tulip) module',
      long_description=README + '\n\n' + CHANGES,
      classifiers=["Development Status :: 2 - Pre-Alpha",
                   "Environment :: Web Environment",
                   'Intended Audience :: Developers',
                   'License :: OSI Approved :: MIT License',
                   "Operating System :: POSIX :: Linux",
                   'Programming Language :: Python',
                   "Topic :: Internet",
                  ],
      platforms=['Linux'],
      author='David Moore',
      author_email='david@linuxsoftware.co.nz',
      url='www.linuxsoftware.co.nz',
      license='MIT',
      keywords='web asyncio tulip socketio',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      entry_points="""\
      [paste.server_runner]
      paster = tulipfest.server:serve_paste
      """, ### THIS IS PROB BORKEN!!!
     )
