TulipFest
==========

An exploration of the new AsyncIO (aka Tulip) module in Python 3.4, along with
WebSockets by Aymeric Augustin, and possibly a simple auth framework on top of
these.
